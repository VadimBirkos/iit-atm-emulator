﻿using System.Data.Entity;
using BLL.EFImplementation.Services;
using BLL.Interface.Services;
using DAL.EFImplementation;
using DAL.EFImplementation.Concrete;
using DAL.Interface;
using DAL.Interface.Repository;
using Ninject;
using Ninject.Web.Common;

namespace DependencyResolver
{
    public static class ResolverConfig
    {
        public static void ConfigureWeb(this IKernel kernel)
        {
            Configure(kernel, true);
        }

        public static void ConfigureConsole(this IKernel kernel)
        {
            Configure(kernel, false);
        }

        private static void Configure(IKernel kernel, bool isWeb)
        {
            
            if (isWeb)
            {
                kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
                kernel.Bind<DbContext>().To<EntityModel>().InRequestScope();
            }
            else
            {
                kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
                kernel.Bind<DbContext>().To<EntityModel>().InSingletonScope();
            }
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IRoleRepository>().To<RoleRepository>();
        }
    }
}
