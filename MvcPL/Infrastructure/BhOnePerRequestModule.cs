﻿using Ninject;
using Ninject.Web.Common;

namespace MvcPL.Infrastructure
{
    public class BhOnePerRequestModule : GlobalKernelRegistrationModule<OnePerRequestHttpModule>
    {
    }
}