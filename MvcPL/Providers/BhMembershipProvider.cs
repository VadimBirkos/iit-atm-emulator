﻿using System;
using System.Web.Helpers;
using System.Web.Security;
using BLL.Interface.Entities;
using BLL.Interface.Services;

namespace MvcPL.Providers
{
    public class BhMembershipProvider : MembershipProvider
    {
        private readonly IUserService _userService;

        public BhMembershipProvider()
        {
            _userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
        }

        public BhMembershipProvider(IUserService userService)
        {
            _userService = userService;
        }

        public MembershipUser CreateUser(string login, string password)
        {
            var user = new User()
            {
                Login = login,
                Password = Crypto.HashPassword(password),
                Role = new Role() { Id = 1 },
                Created = DateTime.Now
            };
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            try { svc.Create(user); }
            catch (InvalidOperationException) { return null; }
            var membershipUser = new MembershipUser("BhMembershipProvider",
                user.Login, null, null, null, null, false, false,
                user.Created, DateTime.MinValue, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue);
            return membershipUser;
        }

        public override bool ValidateUser(string login, string password)
        {
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(login);
            return user != null && Crypto.VerifyHashedPassword(user.Password, password);
        }

        public override MembershipUser GetUser(string login, bool userIsOnline)
        {
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(login);
            if (user == null) return null;
            var memberUser = new MembershipUser("BhMembershipProvider",
                user.Login, null, null, null, null, false, false,
                user.Created, DateTime.MinValue, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue);
            return memberUser;
        }

        #region Stubs
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer,
            bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
            string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override string ApplicationName { get; set; }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}