﻿using System.Web.Mvc;
using System.Web.Security;
using BLL.Interface.Services;
using Ninject;

namespace MvcPL.Providers
{
    public class BhRoleProvider : RoleProvider
    {
        private readonly IUserService _userService;

        public BhRoleProvider()
        {
            _userService = System.Web.Mvc.DependencyResolver.Current.GetService<IUserService>();
          //  _userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
        }

        public BhRoleProvider(IUserService userService)
        {
            _userService = userService;
        }

        public override bool IsUserInRole(string userName, string roleName)
        {
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(userName);
            if (user == null) return false;
            return user.Role.Title == roleName;
        }

        public override string[] GetRolesForUser(string userName)
        {
            var roles = new string[] {};
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(userName);
            if (user == null) return roles;
            if (user.Role != null) roles = new [] {user.Role.Title};
            return roles;
        }

        #region Stubs

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void AddUsersToRoles(string[] userNames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] useNames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string userNameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName { get; set; }

        #endregion
    }
}