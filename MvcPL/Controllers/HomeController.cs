﻿using System;
using System.Linq;
using System.Web.Mvc;
using BLL.Interface.Entities;
using BLL.Interface.Services;

namespace MvcPL.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            var user = _userService.GetAll().First();
            var role = user.Role;
            _userService.Create(new User(){AvatarPath = null, Created = DateTime.Now, Login = "newlogin"+DateTime.Now, Password = "newpass", Role = role});
            var users = _userService.GetAll();
            return View();
        }
    }
}
