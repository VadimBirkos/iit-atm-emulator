﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcPL.ViewModels
{
    public class RegisterViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Login:")]
        [Required(ErrorMessage = "You must input login!")]
        public string Login { get; set; }

        [Required(ErrorMessage = "You must input password!")]
        [StringLength(100, ErrorMessage = "Password must contain {2} or more symbols.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password:")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm your password!")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password:")]
        [Compare("Password", ErrorMessage = "Password must be same!")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Date)]
        public DateTime AddedDate { get; set; }

        [Required]
        public string Captcha { get; set; }
    }
}