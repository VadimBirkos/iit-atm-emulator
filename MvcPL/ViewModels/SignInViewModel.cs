﻿using System.ComponentModel.DataAnnotations;

namespace MvcPL.ViewModels
{
    public class SignInViewModel
    {
        [Display(Name = "Login:")]
        [Required(ErrorMessage = "You must input login!")]
        //  [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный логин")]
        public string Login { get; set; }

        [Required(ErrorMessage = "You must input password!")]
        [StringLength(100, ErrorMessage = "Password must contain {2} or more symbols.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password:")]
        public string Password { get; set; }

        [Display(Name = "Remember?")]
        public bool RememberMe { get; set; }
    }
}