﻿using System;
using System.Linq;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using DependencyResolver;
using Ninject;

namespace ConsolePL
{
    class Program
    {
        private static readonly IKernel _resolver;

        static Program()
        {
            _resolver = new StandardKernel();
            _resolver.ConfigureConsole();
        }

        static void Main(string[] args)
        {
            //ConfigreAppDataDirectory();
            var userService = _resolver.Get<IUserService>(); 
            var user = new User()
            {
                Login = "showmedrama",
                Created = DateTime.Now,
            };
            userService.Create(user);
            
            var users = userService.GetAll().ToList();
           // userService.Delete(users[0]);
            //var updateUser = users[0];
            //updateUser.AdditionalInfo = "new additional info";
            //userService.Update(updateUser);
            //users = userService.GetAll().ToList();
            var publicationService = _resolver.Get<IPublicationService>();
            var publication = new Publication()
            {
                Author = users[0],
                Text = "My first publication is so interesting",
                Title = "Publication title",
               // UserId = users[0].Id
            };
            publicationService.Create(publication);
            users = userService.GetAll().ToList();
            var publications = publicationService.GetAll().ToList();
            var a = publicationService.GetByPredicate(m => m.Author.Login == "showmedrama").ToList();
            var comment = new Comment()
            {
                Author = users[0],
                Posted = DateTime.Now,
                Text = "tak sebe",
                PublicationId = publications[0].Id
            };
            var commentService = _resolver.Get<ICommentService>();
           // for (int i = 0; i < 1000; i++)
           //     commentService.AddComment(comment, publications[0].Id);
            for (int i = 0; i < 100; i++)
                commentService.Create(comment);
                
            var comments = commentService.GetAll().ToList();
            var comsForPub = commentService.GetForPubliation(publications[0].Id).ToList();
            var comsEarlierThan = commentService.GetEarlierThan(comments[156].Posted).ToList();
            //publicationService.Create(publication);
            //publicationService.Delete(publications[0]);
           // userService.Delete(users[0]);
            publications = publicationService.GetAll().ToList();
            //var updPublication = publications[0];
            //updPublication.Text = "edited";
            //publicationService.Update(updPublication);
            publications = publicationService.GetAll().ToList();
            users = userService.GetAll().ToList();
        }

        private static void ConfigreAppDataDirectory()
        {
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            int index = baseDir.IndexOf("BlogHost", System.StringComparison.Ordinal);
            string dataDir = baseDir.Substring(0, index) + @"BlogHost\Data Directory";
            AppDomain.CurrentDomain.SetData("DataDirectory", dataDir);
        }
    }
}
