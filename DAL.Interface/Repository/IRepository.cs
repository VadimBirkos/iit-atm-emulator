﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CommonInterfaces;

namespace DAL.Interface.Repository
{
    public interface IRepository<TEntity>
        where TEntity : IEntity
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int key);
        IEnumerable<TEntity> GetByPredicate(Expression<Func<TEntity, bool>> predicate);
        void Create(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
        //IUnitOfWork UnitOfWork { get; }
    }
}
