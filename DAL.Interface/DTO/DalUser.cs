﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CommonInterfaces;

namespace DAL.Interface.DTO
{
    [Table("Users")]
    public class DalUser : IEntity
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public virtual DalRole Role { get; set; }

        public DateTime Created { get; set; }
    }
}
