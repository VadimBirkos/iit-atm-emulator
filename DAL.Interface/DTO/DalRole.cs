﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterfaces;

namespace DAL.Interface.DTO
{
    [Table("Roles")]
    public class DalRole : IEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
