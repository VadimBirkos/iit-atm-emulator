﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using DAL.Interface;
using DAL.Interface.DTO;
using DAL.Interface.Repository;

namespace BLL.EFImplementation.Services
{
    public class UserService : IUserService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _context;

        public UserService(IRoleRepository roleRepository, 
            IUserRepository userRepository, IUnitOfWork context)
        {
            _roleRepository = roleRepository;
            _userRepository = userRepository;
            _context = context;
        }

        public IEnumerable<User> GetByPredicate(Expression<Func<User, bool>> predicate)
        {
            return GetAll().Where(predicate.Compile());
        }

        public User GetById(int key)
        {
            var dalEntity = _userRepository.GetById(key);
            return CreateBllUser(dalEntity); ;
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll().Select(CreateBllUser);
        }

        public virtual User GetByLogin(string login)
        {
            var userInDb = _userRepository
                           .GetByPredicate(u => u.Login == login)
                           .FirstOrDefault();
            if (userInDb == null) return null;
            return CreateBllUser(userInDb);
        }

        public void Create(User user)
        {
            var userInDb = GetByLogin(user.Login);
            if (userInDb != null)
                throw new InvalidOperationException("User already in database");
            var dalUser = CreateDalUser(user);
            _userRepository.Create(dalUser);
            _context.Commit();
        }

        public void Delete(User user)
        {
            var dalUser = CreateDalUser(user);
            _userRepository.Delete(dalUser);
            _context.Commit();
        }

        public void Update(User user)
        {
            var dalUser = CreateDalUser(user);
            _userRepository.Update(dalUser);
            _context.Commit();
        }

        private DalUser CreateDalUser(User user)
        {
            return new DalUser()
            {
                Login = user.Login,
                Password = user.Password,
                Created = user.Created,
                Role = _roleRepository.GetById(user.Role.Id)
            };
        }

        private User CreateBllUser(DalUser dalUser)
        {
            return new User()
            {
                Login = dalUser.Login,
                Password = dalUser.Password,
                Created = dalUser.Created,
                Role = new Role()
                {
                    Id = dalUser.Role.Id,
                    Title = dalUser.Role.Title
                }
            };
        }
    }
}
