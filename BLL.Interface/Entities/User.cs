﻿using System;
using CommonInterfaces;

namespace BLL.Interface.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public Role Role { get; set; }

        public string AvatarPath { get; set; }

        public DateTime Created { get; set; }
    }
}
