﻿using CommonInterfaces;

namespace BLL.Interface.Entities
{
    public class Role : IEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }
    }
}
