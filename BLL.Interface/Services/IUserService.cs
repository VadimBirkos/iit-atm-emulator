﻿using BLL.Interface.Entities;

namespace BLL.Interface.Services
{
    public interface IUserService : IService<User>
    {
        User GetByLogin(string login);
    }
}
