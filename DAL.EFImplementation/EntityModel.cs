﻿using System.Data.Entity;
using DAL.Interface.DTO;

namespace DAL.EFImplementation
{
    public class EntityModel : DbContext
    {
        public EntityModel()
            : base("AtmEntityModel") { }

        public virtual DbSet<DalUser> Users { get; set; }
        public virtual DbSet<DalRole> Roles { get; set; }
    }
}
