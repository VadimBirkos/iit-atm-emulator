﻿using System.Data.Entity;
using DAL.Interface.DTO;
using DAL.Interface.Repository;

namespace DAL.EFImplementation.Concrete
{
    public class UserRepository 
        : BaseRepository<DalUser>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        }
    }
}
