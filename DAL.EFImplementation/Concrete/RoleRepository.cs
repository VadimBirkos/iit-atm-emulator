﻿using System.Data.Entity;
using DAL.Interface.DTO;
using DAL.Interface.Repository;

namespace DAL.EFImplementation.Concrete
{
    public class RoleRepository 
        : BaseRepository<DalRole>, IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {
        }
    }
}
