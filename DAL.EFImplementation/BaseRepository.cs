﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using CommonInterfaces;
using DAL.Interface.Repository;

namespace DAL.EFImplementation
{
    public class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected readonly DbContext Context;

        public BaseRepository(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");
            Context = context;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Context.Set<TEntity>().AsEnumerable();
        }

        public virtual TEntity GetById(int key)
        {
            return Context.Set<TEntity>()
                            .FirstOrDefault(entity => entity.Id == key);
        }

        public virtual IEnumerable<TEntity> GetByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            return Context.Set<TEntity>().Where(predicate);
        }

        public virtual void Create(TEntity entity)
        {
            DbEntityEntry<TEntity> entry = Context.Entry<TEntity>(entity);
            Context.Set<TEntity>().Add(entry.Entity);
        }

        public virtual void Delete(TEntity entity)
        {
            var dbEntity = Context.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            if (dbEntity != null) Context.Set<TEntity>().Remove(dbEntity);
        }

        public void Update(TEntity entity)
        {
            var dbEntity = Context.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            if (dbEntity != null) Context.Entry((object) dbEntity).CurrentValues.SetValues(entity);
        }
    }
}
